const defaults = {
  CONTEXT: process.env.REACT_APP_CONTEXT,
  API_URL: process.env.REACT_APP_API_URL,
  IMGUR_API_KEY: process.env.REACT_APP_IMGUR_CLIENT_ID,
  GIPHY_API_KEY: process.env.REACT_APP_GIPHY_API_KEY
};

const mapping = {
  development: {
    ...defaults,
    API_URL: '/' // We instead utilize the "proxy" param in package.json
  },
  staging: {
    ...defaults
  },
  production: {
    ...defaults
  }
};

export default mapping[process.env.REACT_APP_CONTEXT];
