import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

import useAuthentication from './lib/authenticationHook';
import AuthenticatedApp from './components/AuthenticatedApp';
import UnauthenticatedApp from './components/UnauthenticatedApp';

function App() {
  const [user, loading, error] = useAuthentication();
  let content = <UnauthenticatedApp />;

  if (loading) {
    content = (
      <div>
        <CircularProgress />
        <Typography variant="caption">Loading...</Typography>
      </div>
    );
  }
  else if (user !== null) {
    content = <AuthenticatedApp />;
  }
  else if (error) {
    console.error(error);
  }

  return (
    <>
      <div data-netlify-identity-button />
      {content}
    </>
  );
}

export default App;
