import { useEffect } from 'react';
import ClipboardJS from 'clipboard';

export const useClipboard = (id) => {
  useEffect(() => {
    new ClipboardJS(id);
  }, [id]);
};
