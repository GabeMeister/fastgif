import { useState } from 'react';

const useAuthentication = () => {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  window.netlifyIdentity.on('init', user => {
    setLoading(false);
  });
  window.netlifyIdentity.on('login', user => {
    setUser(user);
    setLoading(false);
  });
  window.netlifyIdentity.on('logout', () => {
    window.location.reload();
  });
  window.netlifyIdentity.on('error', err => {
    setLoading(false);
    setError(err);
  });

  return [user, loading, error];
};
export default useAuthentication;
