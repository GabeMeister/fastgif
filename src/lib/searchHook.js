import { useState, useEffect } from 'react';

import getGiphyGifs from './giphy';
import getImgurGifs from './imgur';

export const useGifSearch = ({ text, api }) => {
  const [gifResults, setGifResults] = useState([]);
  const [error, setError] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (text.length) {
      setLoading(true);
      setGifResults([]);

      const queryApi = api === 'giphy'
        ? getGiphyGifs
        : getImgurGifs;

      queryApi(text)
        .then(gifData => {
          setGifResults(gifData);
          setLoading(false);
          setError(false);
        })
        .catch(err => {
          setLoading(false);
          setError(true);
        });
    }
    else {
      setGifResults([]);
      setError(false);
    }
  }, [text, api]);

  return [gifResults, loading, error];
};
