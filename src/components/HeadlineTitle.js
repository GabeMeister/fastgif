import styled from 'styled-components';

const HeadlineTitle = styled.div`
  margin-top: 120px;
  text-align: center;
`;
export default HeadlineTitle;
