import React, { useState, useEffect } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import SearchboxWrapper from './wrappers/SearchboxWrapper';
import SearchResultWrapper from './wrappers/SearchResultWrapper';
import ErrorMessage from './ErrorMessage';
import ImagePreview from './ImagePreview';
import HeadlineTitle from './HeadlineTitle';
import { useGifSearch } from '../lib/searchHook';
import { useClipboard } from '../lib/clipboardHook';
import config from '../config';

const AuthenticatedApp = () => {
  const [api, setApi] = useState('giphy');
  const [searchText, setSearchText] = useState('');
  const [results, loading, error] = useGifSearch({ text: searchText, api });
  useClipboard('.js-img-preview');

  useEffect(() => {
    fetch(`${config.API_URL}data`)
      .then(resp => resp.json())
      .then(data => {
        console.log('received data: ', data);
      });
  }, []);

  return (
    <div>
      <HeadlineTitle>
        <Typography variant="h2">Fast Gif (logged in)</Typography>
      </HeadlineTitle>
      <SearchboxWrapper>
        <RadioGroup row>
          <FormControlLabel
            checked={api === 'giphy'}
            value="giphy"
            control={<Radio />}
            label="Giphy"
            onChange={() => {
              setApi('giphy');
            }}
          />
          <FormControlLabel
            checked={api === 'imgur'}
            value="imgur"
            control={<Radio />}
            label="Imgur"
            onChange={() => {
              setApi('imgur');
            }}
          />
        </RadioGroup>
        <TextField
          id="searchbox"
          label="Search for gifs"
          variant="outlined"
          onChange={evt => {
            setSearchText(evt.target.value);
          }}
          fullWidth
          autoFocus
        />
      </SearchboxWrapper>
      <SearchResultWrapper>
        {loading && (
          <CircularProgress />
        )}
        {error && (
          <ErrorMessage />
        )}
        {!!results.length && results.slice(0, 20).map(gif => {
          return (
            <a href={gif.url} key={gif.url} target="_blank" rel="noopener noreferrer">
              <ImagePreview
                className="js-img-preview"
                src={gif.thumbnail}
                data-clipboard-text={gif.url}
              />
            </a>
          );
        })}
      </SearchResultWrapper>
    </div>
  );
};
export default AuthenticatedApp;
